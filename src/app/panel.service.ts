import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PanelService {
  data = {};
  component = {};
  itemUpdated$: EventEmitter<any> = new EventEmitter();

  constructor(
  ) { }

  observe() {
    return this.itemUpdated$;
  }

  open(key, component, data) {
    this.component[key] = component;

    this.itemUpdated$.emit({key,open:true,data});
  }

  close(key) {
    this.itemUpdated$.emit({key,open:false});
  }
}
