import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-panel-header',
  templateUrl: './panel-header.component.html',
  styleUrls: ['./panel-header.component.scss']
})
export class PanelHeaderComponent implements OnInit {
  faTimes = faTimes;

  @Input() title;
  @Input() description;
  @Output() onClose =  new EventEmitter;

  constructor() { }

  ngOnInit() {
  }

  close() {
    this.onClose.emit();
  }
}
