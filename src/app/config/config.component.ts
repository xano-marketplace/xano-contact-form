import { Component, OnInit } from '@angular/core';
import { PanelComponent } from '../panel/panel.component';
import { PanelService } from '../panel.service';
import { FormService } from '../form.service';
import { ConfigService } from '../config.service';
import { Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { XanoService } from '../xano.service';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent extends PanelComponent {
  static ID = Symbol();
  setupForm;
  saving;

  constructor(
    protected panel: PanelService,
    public config: ConfigService,
    protected form: FormService,
    protected api: ApiService,
    protected toast: ToastService,
    protected xano: XanoService
  ) { 
    super(panel);
  }

  onOpen() {
    this.setupForm = this.createForm();
  }

  createForm() {
    return this.form.createFormGroup({
      obj: {
        xanoApiUrl: ['text', [Validators.required, Validators.pattern('^https?://.+')]],
      },
      defaultValue: {
        ...this.config
      }
    });
  }

  getComponentId() {
    return ConfigComponent.ID;
  }

  static open(panel: PanelService, args: { }) {
    panel.open(this.ID, 'Setup', {
      ...args
    });
  }

  save() {
    if (!this.setupForm.trySubmit()) return;

    this.api.get({
      endpoint: this.xano.getApiSpecUrl(this.setupForm.value.xanoApiUrl),
      headers: {
        Accept: 'text/yaml'
      },
      responseType: "text",
      stateFunc: state => this.saving = state
    }).subscribe((res:any) => {
      if (!this.config.requiredApiPaths.every(path => res.includes(path))) {
        this.toast.error("This Xano Base URL is missing the required endpoints. Have you installed this marketplace extension?");
        return;
      }

      this.config.xanoApiUrl = this.xano.getApiUrl(this.setupForm.value.xanoApiUrl);
      this.close();
    }, err => {
      this.toast.error("An error has occured.");
    });
  }
}
